import React, { Component }from 'react';
import './Button.css';

class Button extends Component{
    isOperator = (val) => {
        if(!isNaN(val) || val === '.' || val === '=' )
        return true;
        else
        return false
    }

    render(){
        return (<div className = {this.isOperator(this.props.children) ? "button" : "operator"}
            onClick={() => this.props.handleClick(this.props.children)}>{this.props.children}</div>)
    }
}
export default Button;