import React, {Component} from 'react';
import './App.css';
import Button from './Component/Button'
import Input from './Component/Input'
import ClearButton from './Component/ClearButton'

class App extends Component {
  constructor(props){
    super()
    this.state = {
      previousNumber: "",
      currentNumber: "",
      input: "",
      operator: ""
    }
  }
  addToInput = val => {
    this.setState({
      input: this.state.input+val
    });
    // console.log(val)
  }
  clearInput = () => {
    this.setState({input: ""})
  }
  addDecimal = val => {
    if(this.state.input.indexOf(".") === -1)
    this.setState({input: this.state.input + val})
  }
  addZero = val => {
    if(this.state.input !== "")
    this.setState({input: this.state.input + val})
  }
  divide = () => {
    this.setState({
      previousNumber: this.state.input,
      input: "",
      operator: "divide"
    })
  }
  multiplication = () => {
    this.setState({
      previousNumber: this.state.input,
      input: "",
      operator: "multiplication"
    })
  }
  subtract = () => {
    this.setState({
      previousNumber: this.state.input,
      input: "",
      operator: "subtract"
    })
  }
  sum = () => {
    this.setState({
      previousNumber: this.state.input,
      input: "",
      operator: "sum"
    })
  }
  evaluate = () => {
    this.state.currentNumber = this.state.input;

    if (this.state.operator === "sum") {
      this.setState({
        input:
          parseFloat(this.state.previousNumber) +
          parseFloat(this.state.currentNumber)
      });
    } else if (this.state.operator === "subtract") {
      this.setState({
        input:
          parseFloat(this.state.previousNumber) -
          parseFloat(this.state.currentNumber)
      });
    } else if (this.state.operator === "multiplication") {
      this.setState({
        input:
          parseFloat(this.state.previousNumber) *
          parseFloat(this.state.currentNumber)
      });
    } else if (this.state.operator === "divide") {
      this.setState({
        input:
          (parseFloat(this.state.previousNumber) /
          parseFloat(this.state.currentNumber)).toFixed(2)
      });
    }
    this.setState({
      currentNumber: "",
      operator: ""
    })
  }
  render(){
    return (
      <div className="App">
        <div className = "Container">
          <div className= "row">
            <Input>{this.state.input}</Input>
          </div>
          <div className = "row">
            <Button handleClick={this.addToInput}>7</Button>
            <Button handleClick={this.addToInput}>8</Button>
            <Button handleClick={this.addToInput}>9</Button>
            <Button handleClick={this.divide}>/</Button>
          </div>
          <div className = "row">
            <Button handleClick={this.addToInput}>4</Button>
            <Button handleClick={this.addToInput}>5</Button>
            <Button handleClick={this.addToInput}>6</Button>
            <Button handleClick={this.multiplication}>*</Button>
          </div>
          <div className = "row">
            <Button handleClick={this.addToInput}>1</Button>
            <Button handleClick={this.addToInput}>2</Button>
            <Button handleClick={this.addToInput}>3</Button>
            <Button handleClick={this.subtract}>-</Button>
          </div>
          <div className = "row">
            <Button handleClick={this.addDecimal}>.</Button>
            <Button handleClick={this.addZero}>0</Button>
            <Button handleClick={this.evaluate}>=</Button>
            <Button handleClick={this.sum}>+</Button>
          </div>
          <ClearButton handleClick = {this.clearInput}>Clear</ClearButton>
        </div>
      </div>
    );
  }
}

export default App;
